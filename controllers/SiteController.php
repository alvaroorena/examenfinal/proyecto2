<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    
    
    
    public function actionConsultaexamen2dao(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT DISTINCT c.nombre, c.dorsal  FROM etapa e INNER JOIN ciclista c ON e.dorsal = c.dorsal WHERE e.dorsal NOT IN (SELECT DISTINCT l.dorsal FROM lleva l INNER JOIN maillot m ON l.código = m.código WHERE m.color ="rosa")',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['nombre', 'dorsal'],
           "titulo"=>"Consulta 2 del examen con DAO",
           "enunciado"=>"Los nombres y dorsales de los ciclistas que habiendo ganado etapas, nunca han llevado el maillot de color rosa",
           "sql"=>"SELECT DISTINCT c.nombre, c.dorsal  FROM etapa e INNER JOIN ciclista ON e.dorsal = c.dorsal WHERE e.dorsal NOT IN (SELECT DISTINCT l.dorsal FROM lleva l INNER JOIN maillot m ON l.código = m.código WHERE m.color ='rosa'",
           "explicacion"=>"Sacamos nombres y dorsales de ciclistas que han ganado etapas, dorsales de ciclistas que han llevado maillot rosa, y hacemos una resta de conjuntos con los dorsales "
       ]);
        
        
    }
    
    
    public function actionConsultaexamen3orm(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Etapa::find()
                ->select("SUM(kms) AS kmtotales  "),
                
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
        return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['kmtotales'],
           "titulo"=>"Consulta 3 del examen con ORM",
           "enunciado"=>"Número total de kilómetros recorridos por un ciclista",
           "sql"=>"SELECT SUM(e.kms) AS kmtotales  FROM etapa e;",
           "explicacion"=>"Utilizamos la función SUM, para obtener la suma de todos los kilómetros de las etapas y le damos el alias de kmtotales, todo ello de la tabla etapa"
            
       ]);
        
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
